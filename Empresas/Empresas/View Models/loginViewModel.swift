//
//  loginViewModel.swift
//  Empresas
//
//  Created by victoredbc on 07/01/22.
//

import Foundation
import SwiftUI

class loginViewModel: ObservableObject {
    
    static func getUser() -> userAuth{
        userAuth(email: "ola", password: 1)
    }
    
    @Published var model: userAuth = getUser()
    @Published var userEmail = ""
    @Published var userPassword = ""
    
    @State var isLogedin = false
    
    var users: Array<userAuth.user> {
        model.users
    }
    
    static func keyboardAvoidance() {
        print("keyboard -> view up")
    }

    var textFieldValidationColor: String {
        if !isLoginValid {
            return "empresasPrimaryGray"
        }
        else {
            return "warningRed"
        }
    }
    
    func showTextFieldName(textFieldName: String, isClicked: Bool) -> String {
        if isLoginValid {
            return textFieldName
        }
        else {
            return ""
        }
    }
    
    //MARK: - main login method

    static func login() {
        //
    }
    
    //MARK: - Validation funcs
    
    var isLoginValid: Bool {
        if !isEmailValid() {
            return false
        }
        else {
            return true
        }
    }
    
    func isEmailValid() -> Bool {
        let emailTest = NSPredicate(format: "SELF MATCHES %@","^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
        return emailTest.evaluate(with: userEmail)
    }
    
}



