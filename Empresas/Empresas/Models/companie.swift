//
//  companie.swift
//  Empresas
//
//  Created by victoredbc on 08/01/22.
//

import Foundation

struct companiesApp {
    struct companie {
        var id: Int
        var enterpriseName: String
        var photo: String
        var descipition: String
        var enterpriseType: String
    }
    
    private(set) var companies: Array<companie>
    
    init() {
        companies = Array<companie>()
    }
} 



let JSON = """
 {
     "enterprises": [
         {
             "id": 6,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Veuno Ltd.",
             "photo": "/uploads/enterprise/photo/6/240.jpeg",
             "description": "Problem identified: People have difficulties managing money and coordinating across multiple accounts.\n\nSolution: Bring all accounts in one place for easier management using a goal-based approach and third party service recommendations for efficient spending, everything inside a mobile app.\n\nOur goal is to help people get more value from their money.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 9,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Invest In Crowd Ltd",
             "photo": "/uploads/enterprise/photo/9/240.jpeg",
             "description": "We have an equity only crowdfunding platform for property developers and investors.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 11,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Investment Searcher",
             "photo": "/uploads/enterprise/photo/11/240.jpeg",
             "description": "An automated matching making service for investors from around the world and entrepreneurs looking for finance. The tech is built and the data is collected. The company will launch in the next two months. This is an investment in a company that finds its competition investment!",
             "city": "Cardiff",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 18,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Assetvault",
             "photo": "/uploads/enterprise/photo/18/240.jpeg",
             "description": "AssetVault provides digital legal and financial products to large Fiancial Institutions to help them get a deeper share of wallet from existing customers and acquire new customers",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 21,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Moniday Ltd",
             "photo": "/uploads/enterprise/photo/21/240.jpeg",
             "description": "Wealth Management for mass market within retail sector",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 24,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "World Wide Generation",
             "photo": "/uploads/enterprise/photo/24/240.jpeg",
             "description": "World Wide Generation is creating a Distributed Ledger Ecosystem, G17Eco, to improve our world by validating and reporting on the impact of global social investment.\n\n\nG17Eco will  map and measure the financial and social impact of investments around the world against the UN’s 17 Sustainable Development Goals. It will capture independently validated data in a secure blockchain database to assist government, philanthropic, corporates, and individual investors in gaining confidence in their choice of direct investment and it is having maximum beneficial effect.\n",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 29,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "DealGlobe",
             "photo": "/uploads/enterprise/photo/29/240.jpeg",
             "description": "DealGlobe is a technology driven advisory firm specialised on China cross-border M&A and capital raising. ",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 44,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Lendhaus",
             "photo": "/uploads/enterprise/photo/44/240.jpeg",
             "description": "Turbocharging the refinancing of commercial property.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         }
     ]
 }
"""
/*
let jsonData = JSON.data(using: .utf8)!
let blogPost: companiesApp = try! JSONDecoder().decode(companiesApp.companie.self, from: jsonData)
*/
