//
//  LoginView.swift
//  Empresas
//
//  Created by victoredbc on 06/01/22.
//

import SwiftUI

struct loginView: View {
    @ObservedObject var loginControl = loginViewModel()
    
    var body: some View {
        VStack {
            HStack {
                VStack() {
                    Spacer()
                    welcomingTextLogin()
                }
                Spacer()
            }
            .padding()
            .background(Color(.purple))
            .frame(height: UIScreen.main.bounds.height * 0.58)
            
            VStack {
                loginLabel
                textFieldsLogin(content: $loginControl.userEmail, placeholderText: "Email", textFieldColor: loginControl.textFieldValidationColor,textFieldName: "Email")
                textFieldsLogin(content: $loginControl.userPassword, placeholderText: "Senha", textFieldColor: loginControl.textFieldValidationColor, textFieldName: "Senha")
                Spacer()
                buttonLogin
                Spacer()
            }
            .padding()
        }
        .frame(maxHeight: .infinity)
    }
}

struct welcomingTextLogin: View {
    var body: some View {
        Text("Boas vindas,")
            .font(.system(size: 40, weight: .bold, design: .default))
            .frame(alignment: .leading)
            .foregroundColor(Color("titleColorWhite"))
        Text("Você está no Empresas.")
            .font(.system(size: 24, weight: .thin, design: .default))
            .foregroundColor(Color("titleColorWhite"))
    }
}

struct textFieldsLogin: View {
    @Binding var content: String
    @State var isClicked: Bool = false
    var placeholderText: String
    var textFieldColor: String
    var textFieldName: String
    var body: some View {
        VStack{
            if isClicked {
                HStack {
                    Text(textFieldName)
                        .font(.system(size: 12, design: .default))
                        .foregroundColor(Color("empresasPrimaryBlack"))
                    Spacer()
                }
            }
            TextField(placeholderText, text: $content)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(Color(textFieldColor), lineWidth: 1)
                )
                .onTapGesture {
                    loginViewModel.keyboardAvoidance()
                    self.isClicked = true
                }
                .ignoresSafeArea(.keyboard)
                .autocapitalization(.none)
            /*
            if isClicked {
                HStack {
                    Text("Endereço de email invalido")
                        .font(.system(size: 12, design: .default))
                        .foregroundColor(Color("empresasPrimaryBlack"))
                    Spacer()
                }
            }
             */
        }
    }
}

var buttonLogin: some View {
    Button(action: {
        //self.loginControl.isSignUpComplete
    }, label: {
        Text("Entrar")
            .padding()
            .frame(maxWidth: .infinity)
            .background(Color(red: 0.542, green: 0.542, blue: 0.542))
            .cornerRadius(25)
            .foregroundColor(.white)
    })
        .padding(.top)
}

var loginLabel: some View {
    HStack {
        Text("Digite seus dados para continuar.")
            .font(.system(size: 16, weight: .bold, design: .default))
        Spacer()
    }
    .padding(.bottom)
}

struct loginView_Previews: PreviewProvider {
    static var previews: some View {
        loginView()
    }
}
