//
//  companiesDetail.swift
//  Empresas
//
//  Created by victoredbc on 08/01/22.
//

import SwiftUI

struct companiesDetail: View {
    var body: some View {
        GeometryReader { view in
        VStack {
            textTitleDetail()
            imageFrameDetail
            scrollViewDescptionDetail()
                .frame(height: view.size.height * 0.5)
        }
        }
    }
}

struct textTitleDetail: View {
    var body: some View {
        VStack {
            Text("McDonald's")
                .font(.system(size: 40, weight: .bold, design: .default))
            Text("Restaurante . Fast-food")
        }
        .padding()
        .background(Color(.purple))
    }
}

var imageFrameDetail: some View {
    Image("exemple")
        .resizable()
}

struct scrollViewDescptionDetail: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: true) {
            Text("McDonald's Corporation é a maior cadeia mundial de restaurantes de fast food de hambúrguer, servindo cerca de 68 milhões de clientes por dia em 119 países através de 37 mil pontos de venda. Com sede nos Estados Unidos, a empresa começou em 1940 como uma churrascaria operada por Richard e Maurice McDonald. Em 1948, eles reorganizaram seus negócios como uma hamburgueria que usava os princípios de uma linha de produção. O empresário Ray Kroc ingressou na empresa como franquiado em 1955.")
            .font(.system(size: 16, design: .default))
            .foregroundColor(Color("empresasSecondaryGrey"))
        }
        .padding()
    }
}

struct companiesDetail_Previews: PreviewProvider {
    static var previews: some View {
        companiesDetail()
    }
}
