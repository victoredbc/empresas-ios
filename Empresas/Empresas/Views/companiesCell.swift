//
//  companiesCell.swift
//  Empresas
//
//  Created by victoredbc on 07/01/22.
//

import SwiftUI

var companieName = "McDonald's"
var image = Image("exemple")

struct companiesCell: View {
    var body: some View {
        GeometryReader { view in
            ZStack(alignment: .bottom) {
                textCompanieName
                imageCompanie
            }
            .frame(minWidth: 0, maxWidth: view.size.width, maxHeight: view.size.height, alignment: .center)
        }
    }
}

struct companiesCell_Previews: PreviewProvider {
    static var previews: some View {
        companiesCell()
    }
}

var textCompanieName: some View {
    Text(companieName)
        .padding(.top)
        .frame(width: 152, height: 107, alignment: .bottom)
        .background(RoundedRectangle(cornerRadius: 16)
                        .fill(Color(.white))
                        .shadow(color: .black, radius: 4, x: 0, y: 3)
        )
}

var imageCompanie: some View {
    image
        .padding(.bottom)
}


