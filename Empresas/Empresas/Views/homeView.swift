//
//  homeView.swift
//  Empresas
//
//  Created by victoredbc on 07/01/22.
//

import SwiftUI

struct homeView: View {
    
    @State public var userSearch: String = ""

    var body: some View {
        VStack {
            textTitleHome()
            //textTitleOngoingSearch()
            textFieldSearch(text: userSearch)
            Spacer()
            scrollViewCompanies
        }
    }
}

struct textTitleHome: View {
    var body: some View {
        HStack {
            Text("Pesquise por uma empresa")
                .font(.system(size: 40, weight: .bold, design: .default))
                .frame(alignment: .leading)
            Spacer()
        }
        .padding()
    }
}

struct textFieldSearch: View {
    @State var text: String
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
            TextField("Buscar...", text: $text)
        }
        .padding()
        .overlay(RoundedRectangle(cornerRadius: 8)
                    .stroke(Color("empresasPrimaryGray"), lineWidth: 1))
        .padding()
    }
}


var scrollViewCompanies: some View {
    GeometryReader { view in
        ScrollView {
            ForEach(/*@START_MENU_TOKEN@*/0 ..< 5/*@END_MENU_TOKEN@*/) { item in
                HStack {
                    ForEach(0..<2) { item in
                        companiesCell()
                    }
                    .frame(width: view.size.width * 0.5, height: view.size.height * 0.33)
                }
            }
        }
        .frame(width: view.size.width)
    }
}

struct textTitleOngoingSearch: View {
    var body: some View {
        HStack {
            Text("Pesquise")
                .font(.system(size: 24, weight: .bold, design: .default))
                .frame(alignment: .leading)
        }
    }
}

struct homeView_Previews: PreviewProvider {
    static var previews: some View {
        homeView()
    }
}
